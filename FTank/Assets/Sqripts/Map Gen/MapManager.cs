﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapManager : MonoBehaviour {

	[SerializeField,Tooltip("Map size")]
	private int rows;
	[SerializeField]
	private int colms;
	[SerializeField]
	private float roomWidth = 50f;
	[SerializeField]
	private float roomHight = 50f;
	[SerializeField]
	private GameObject[] roomPrefabs;
	public bool mapOfTheDay;

	private RoomCode[,] grid;
	public int genorationSeed;

	// Use this for initialization
	void Awake()
	{
		GenerateRoom ();
	}



	public GameObject GrabRandomPrefab()
	{
		return roomPrefabs [UnityEngine.Random.Range (0, roomPrefabs.Length)];
	}

	void GenerateRoom()
	{
		UnityEngine.Random.seed = SeedDate (DateTime.Now);
		grid=new RoomCode[colms,rows];//Write ove the grid positions.
		for(int z =0; z<rows; z++)// with every row
		{
			for(int x=0; x<colms; x++)//for every column
			{// Set the position based on our tile height and width and position
				
				float xPosition = roomWidth * x;
				float zPosition = roomHight * z;
				Vector3 newPosition = new Vector3 (xPosition, 0.0f, zPosition);

				// Start spawning the map
				GameObject tempRoom= Instantiate(GrabRandomPrefab(), newPosition,Quaternion.identity) as GameObject;
				tempRoom.transform.parent = this.transform;//Set parten for next one
				tempRoom.name="Room "+x+","+z;//Set name for Inspector

				RoomCode storetempRoom=tempRoom.GetComponent<RoomCode>(); // Grab room object

				//Open Doors
				if (x != 0) {
					storetempRoom.WestDoor.SetActive (false);//If this room is on the not in the furthest row up open north door 
				} 
				if (x != colms - 1) {
					storetempRoom.EastDoor.SetActive (false);//If this room is at the top open the south door 
				}
				if(z !=0)
				{
					storetempRoom.SouthDoor.SetActive (false);
				}
				if (z != rows - 1) 
				{
					storetempRoom.NorthDoor.SetActive (false);
				}

			//	grid[x,z]=tempRoom;//Save this layout

			}
		}
	}

	public int SeedDate(DateTime todaysDate)
	{
		if (mapOfTheDay == true)
		{
			return todaysDate.Year + todaysDate.Month + todaysDate.Day;
		}
		else
		{
			return todaysDate.Year + todaysDate.Month + todaysDate.Day + todaysDate.Hour + todaysDate.Minute;
		}

	}

	//R.I.P SPD Green and Yellow DAB

}
