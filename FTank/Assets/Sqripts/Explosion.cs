﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	private TankData data;

	public float radius;
	public int exsplosionDamage=10;
	public int explodeLayer;


	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
	}

	public void Explode()
	{
		int layerMask = 1 << explodeLayer;//ONly cast to layer 8
		//Instantiate (effect,transform.position,transform.rotation); Effects
		RaycastHit[] hits = Physics.SphereCastAll (transform.position,radius,transform.forward,Mathf.Infinity,layerMask);

		foreach (RaycastHit hit in hits)
		{
			Takedamage health = hit.collider.gameObject.GetComponent<Takedamage>();
			Debug.Log ("in ex");

			if (health != null)
			{
				Debug.Log ("checking health");
				//Debug.Log ("We got health");
				health.TakeDamage(exsplosionDamage);
			}
			/*if (hit.rigidbody != null)
			{
				hit.rigidbody.AddForce(-hit.normal * hitForce);
			}*/ //If we want to push non-killed tanks.
		}
		Destroy(gameObject);
	}

	void OnTriggerEnter (Collider other )
	{
		Debug.Log ("collid");
		if(other.GetComponent<PlayerControl>())
		{
			Debug.Log ("ex");
			Explode ();
		}
	}

}
