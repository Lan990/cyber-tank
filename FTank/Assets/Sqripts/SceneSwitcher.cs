﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneSwitcher : MonoBehaviour {
	public GameObject options;
	public GameObject pause;


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (pause.activeSelf == true)
			{
				pause.SetActive(false);
			}
			else
			{
				pause.SetActive(true);

			}
		}
	}
	public void OpenOptions()
	{
			if (options.activeSelf == true)
			{
				options.SetActive(false);
			}
			else
			{
				options.SetActive(true);

			}
	}
    public void SwitchScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

	public void Quit()
	{
		Application.Quit();
	}

}
