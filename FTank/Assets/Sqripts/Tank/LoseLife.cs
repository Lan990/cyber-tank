﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseLife : MonoBehaviour {

    void OnDestroy()
    {
        GameManager.gM.playerLives--;
    }
}
