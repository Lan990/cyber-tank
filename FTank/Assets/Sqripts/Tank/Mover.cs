﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	[HideInInspector]
	public CharacterController cC;
	private TankData data;

	// Use this for initialization
	void Start () {

		cC = GetComponent<CharacterController> ();
		data = GetComponent<TankData> ();
	}
	

	//Function to move character 
	public void Move(Vector3 moveTo)//Gets the move vector from player controler
	{
		cC.SimpleMove (moveTo);
	}
    public void Rotate(Vector3 turnTo)//Gets the move vector from player controler
    {
        data.tf.Rotate(turnTo);
    }

    //Function to rotate character 
    public void Rotate(float turnSpeedAndDirection)
    {
        transform.Rotate(new Vector3(0, turnSpeedAndDirection * Time.deltaTime, 0));
    }

    public void RotateTowards(Vector3 newDirection)
    {
        // create variable to hold how we Want to end up turned
        Quaternion goalRotation;
        // set variable to how we need to turn in order to look down newDirection
        goalRotation = Quaternion.LookRotation(newDirection);
        // Rotate from our current rotation towards our target rotation
        transform.rotation = Quaternion.SlerpUnclamped(transform.rotation, goalRotation, data.turnSpeed * Time.deltaTime);
    }


    /*public void Exsplode(gam)
	{
		Destroy()
		//Effects for exsplosion
	}*/

}

