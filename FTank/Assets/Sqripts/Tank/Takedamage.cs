﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Takedamage : MonoBehaviour {
	private TankData data;
	private int health;
    public AudioClip deathSound;
    public AudioClip hitSound;

    // Use this for initialization
    void Awake(){
		data = GetComponent<TankData> ();
	}
	void Start () {
		//Sets yhe health in the script equal to what's in data
		health = data.maxHealth;
	}
	
	//takes away health from tank and destorys it if it reached zero
	void TakeDamage(int DamageTaken)
	{
		health -= DamageTaken;
        data.health = health;
		SoundManeger.sound.source.PlayOneShot (hitSound, 1.0f);

        if (health <= 0) 
		{
//			GameManager.gM.score++;
			Destroy (gameObject);
			SoundManeger.sound.source.PlayOneShot (deathSound, SoundManeger.sound.fxVolume);

        }
    }
}
