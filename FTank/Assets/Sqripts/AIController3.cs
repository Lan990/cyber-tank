﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Personality {Ranged,Kamikaze,Basic}
public enum LoopSet {Loop,PingPong,Stop,Nothing}

public class AIController3 : MonoBehaviour {
	[Header("Tank Components")]
	private TankData data;
	private Mover mover;

	[Header("Tank AI settings")]
	//public CombatSett combatSett;// Veriable for the enum.
	public LoopSet loopSet;
	public Personality personality;
	[Tooltip("Bool to track if we shoud be running.")]
	public bool flee;
	[Tooltip("How long we ")]
	public float fleeTime=5;
	private float countDown;

	[Header("Navigation")]
	[SerializeField]
	private int avoidStep;
	[Tooltip("How far the tank can see")]
	public float viewDistance;
	[Tooltip("how wide the tank can see")]
	public float fieldOfView;
	[Tooltip("How far the tank should get from the target.")]
	public float distanceToPoint;


	private Vector3 turnDes;// Tracks what direction to turn in Avoid().
	private string obsDir;//Tracks what side obsticle is on in Avoid().

	[Header("Check point for tank to patrol")]
	[Tooltip("List of points in the world for AI to move to.")]
	public GameObject[] wayPoints;
	private int currWaypoint;// Spot in world the Tank is trying to get to.

	[Header("Combat")]
	[Tooltip("How far this enemy should go to escape the player")]
	public float fleeDis;
	public int enemyLayerMask=9;



	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
		mover = GetComponent<Mover> ();
		currWaypoint=Random.Range(0,wayPoints.Length-1);
		if(personality==Personality.Kamikaze)
		{
			distanceToPoint = 0;
		}

		wayPoints = GameObject.FindGameObjectsWithTag ("Waypoint");
		GameManager.gM.enemyCount++;
	}
	
	// Update is called once per frame
	void Update () {
		TankRun();

	}

	void TankRun()
	{
		Vector3 moveVector = Vector3.zero;
		moveVector = data.tf.forward * data.moveSpeed;//Move forword
		if (flee) 
		{
			Flee (GameManager.gM.player, moveVector);
		} 
		else 
		{
			if (!CanSeePlayer (GameManager.gM.player)) 
			{

				MoveToCheckpoint (moveVector);
				Debug.Log ("going to checkpoint");

			} 
			else 
			{
				if (data.health <= data.maxHealth / 2) 
				{
					flee = true;
					Debug.Log ("flee");
				} 
				else 
				{
					Chase (moveVector);
					Debug.Log ("chase");
				}
			}
		}
	}

	bool CheckPath(string checkDir)
	{
		int layerMask = 1 << 8;//ONly cast to layer 8
		RaycastHit hit;
		switch(checkDir)
		{
			case "forword":
			if (Physics.Raycast (data.tf.position, data.tf.forward * data.moveSpeed, out hit, viewDistance, layerMask)) { //If raycast iht somthing in the view distence.
					Debug.DrawRay (data.tf.position, data.tf.forward * data.moveSpeed * hit.distance, Color.red);
						return false;
					} else { 
					Debug.DrawRay (data.tf.position, data.tf.forward * data.moveSpeed * 1000, Color.green);
						return true;

					}
				break;

			case "left":
			if (Physics.Raycast (data.tf.position, -data.tf.right * data.moveSpeed, out hit, viewDistance+10, layerMask)) { //If raycast iht somthing in the view distence.
					Debug.DrawRay (data.tf.position, -data.tf.right * data.moveSpeed * hit.distance, Color.red);
						return false;
					} else { 
					Debug.DrawRay (data.tf.position, -data.tf.right * data.moveSpeed * 1000, Color.green);
						return true;

					}
				break;
		case "right":
			if (Physics.Raycast (data.tf.position, data.tf.right * data.moveSpeed, out hit, viewDistance+10, layerMask)) { //If raycast iht somthing in the view distence.
				Debug.DrawRay (data.tf.position, data.tf.right * data.moveSpeed * hit.distance, Color.red);
				return false;
			} else { 
				Debug.DrawRay (data.tf.position, data.tf.right * data.moveSpeed * 1000, Color.green);
				return true;

			}
			break;
		
		default:
			return false;
			Debug.Log ("Default ran");
			break;
		}

	}


	void Avoid()
	{
		
		if (avoidStep == 1) //Where shoould I rotate?
		{
			if (CheckPath ("left")) 
			{
				turnDes =-data.tf.right;//Vector to the left.
				obsDir="right";
				Debug.Log ("left");
				avoidStep = 2;

			} 
			else if (CheckPath ("right"))
			{

				turnDes =data.tf.right;//Vector to th right
				obsDir="left";
				Debug.Log ("right");
				avoidStep = 2;
			} 
			else 
			{
				Debug.Log ("No where to go");
			}
		}
		if (avoidStep==2) //Rotate
		{
			mover.RotTo (turnDes);
				if(obsDir=="right")
				{
					turnDes = data.tf.right;//Vector to turn back to the right
				}
				else if (obsDir == "left") {
					turnDes = -data.tf.right;//Vector to turn back to the left
				} else {
					Debug.Log ("Error in avoid step 2 no turn back set");
				}
				avoidStep = 3;//move to avoid step 3.

				//Nothing let the model rotate.
				Debug.Log ("rot");
			




		}
		//Rotae help from  aldonaletto  on unity answers. 
		if (avoidStep == 3)
		{

			Vector3 moveVector = Vector3.zero;
			moveVector = data.tf.forward * data.moveSpeed;

			if (CheckPath ("forword"))
			{
				mover.Move (moveVector);
				if (obsDir == "right") {
					if (CheckPath (obsDir)) {
						mover.RotTo (turnDes);
							avoidStep = 0;

					}
				}
				else if (obsDir == "left")
				{
					if (CheckPath (obsDir))
					{
						mover.RotTo (turnDes);
						avoidStep = 0;
					}
				}
			} 
	

		}
			
	}
	void MoveToCheckpoint(Vector3 moveVector)
	{


		if (avoidStep == 0)
		{
			
			mover.RotToTarget (wayPoints[currWaypoint].transform.position);
			if (CheckPath("forword")) 
			{
				if (Vector3.SqrMagnitude (wayPoints [currWaypoint].transform.position - data.tf.position) < (distanceToPoint * distanceToPoint))// Only get but so close to the point
				{
					UpdatePatrol ();
				} else {
					mover.Move (moveVector);

				}

			} 
			else if (!CheckPath("forword"))
			{
				avoidStep = 1;
			}
		} 
		else 
		{
			Avoid ();
		}
	}

	bool CanSeePlayer(GameObject player)
	{
		// Checking if target exists
		if (player == null)
		{
			return false;
		}


		float enemysight=viewDistance;
		if(personality==Personality.Ranged)
		{
			enemysight *= 2;
		}
		// Checks if in field of view
		Vector3 vectorToTarget = player.transform.position - data.tf.position;
		if (Vector3.Angle (data.tf.forward, vectorToTarget) <= fieldOfView) {
			RaycastHit info;
			// check if ray cast hits something
			if (Physics.Raycast (data.tf.position, vectorToTarget, out info, enemysight)) {
				if (info.collider.gameObject == player) {
					return true;
				}
			}
		}

		return false;
	}

	void Chase (Vector3 moveVector)
	{
		mover.RotToTarget (GameManager.gM.player.transform.position);
		if (Vector3.SqrMagnitude (GameManager.gM.player.transform.position - data.tf.position) < (distanceToPoint * distanceToPoint)) 
		{
			Shoot ();
			//Just stop

		}
		else if (CheckPath("forword")) {
			mover.Move (moveVector);
			Shoot ();
		}
		else if(!CheckPath("forword")) // Do the first step in avoidnce.
		{
			avoidStep = 1;
		}

	}

	void Shoot()
	{//Checks for the shoot input and checks how long it's been since the last press.
		if (Time.time > data.nextFire)
		{
			//Debug.Log ("Shooting");
			SendMessage ("Fire", SendMessageOptions.DontRequireReceiver);
			data.nextFire = Time.time + data.fireRate;
		}
	}

	void UpdatePatrol()
	{
		if(loopSet==LoopSet.Loop)
		{
			if (currWaypoint == wayPoints.Length-1)
			{
				currWaypoint = 0;
			} 
			else {
				currWaypoint++;
			}
		}
		else if(loopSet==LoopSet.Nothing)
		{
			//Stay still
		}
		else if(loopSet==LoopSet.PingPong)
		{
			currWaypoint = Random.Range (0,wayPoints.Length-1);
		}
		else if(loopSet==LoopSet.Stop)//Stop at last way point
		{
			if (currWaypoint != wayPoints.Length-1)
			{
				currWaypoint++;
			} else
			{
				// Don't give new waypoint
			}
		}
	}

	void Flee(GameObject player,Vector3 moveVector)
	{
		Vector3 vectorToTarget=Vector3.zero;
		vectorToTarget= player.transform.position - data.tf.position;//Move directly to target
		Vector3 VectorFromTarget=Vector3.zero;
		VectorFromTarget =-1*vectorToTarget;//-1 flips the vector in the opposit direction
		VectorFromTarget.Normalize();//Gives a normal magnatude of one.
		VectorFromTarget*=fleeDis;
		Vector3 fleetoo=VectorFromTarget+data.tf.position;
		mover.RotToTarget (fleetoo);
		if (avoidStep == 0) {
			
			if (CheckPath ("forword")) {
				mover.Move (moveVector);
			} else {
				avoidStep = 1;
			}
		} else {
			Avoid ();
		}

		countDown+=Time.deltaTime;
		if(countDown>=fleeTime)
		{
			flee = false;
			countDown = 0;
		}
	}


}
 