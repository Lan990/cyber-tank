﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum LoopSet {Loop,PingPong,Stop,}

public class AIController1 : MonoBehaviour {
	private TankData data;
	private Mover mover;

	//List of points in the world for AI to move to.
	public List<Transform> wayPoints;
	private int currWaypoint;// Spot in world the Tank is trying to get to.

	[SerializeField]
	private float distanceToPoint;//How close should we get to the way point.

	public LoopSet loopSet;// Veriable for the enum.

	void Awake()
	{
		data = GetComponent<TankData> ();
		mover = GetComponent<Mover> ();
	}

	// Use this for initialization
	void Start () {
		
		currWaypoint = (int)Mathf.Clamp (currWaypoint, 0, wayPoints.Count - 1);//Selects a waypoint out of list.
	}
	
	// Update is called once per frame
	void Update () {
		MoveInput ();


		//What i was doing changing rotate to work with v3
	}




	void MoveInput()
	{
		Vector3 moveVector = Vector3.zero;
		moveVector = data.tf.forward * data.moveSpeed;
		bool patrolForward = true;


		if (mover.RotateTo (wayPoints [currWaypoint].position)) 
		{
			//Don't do anything.
		}
		else 
		{
			SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);
		}
		if (Vector3.SqrMagnitude (wayPoints [currWaypoint].position - data.tf.position) < (distanceToPoint * distanceToPoint))//Find how close we are to the point and if we're within the desired ditance.
		{
			if (loopSet == LoopSet.Stop) { //If loop seeting is set to stop
				//Go to the next check point.
				if (currWaypoint < wayPoints.Count - 1) {
					currWaypoint++;
				}
			} else if (loopSet == LoopSet.Loop) {
				//Go to the next check point.
				if (currWaypoint < wayPoints.Count - 1) {
					currWaypoint++;
				} else { //If we're at the last one
					currWaypoint = 0; //Go back to begining
				}

			} 
			else if (loopSet == LoopSet.PingPong) 
			{
				if (patrolForward) {
					if (currWaypoint < wayPoints.Count - 1) {
						currWaypoint++;
					} else {
						patrolForward = false;
						currWaypoint--;
					}
				}
				else 
				{
					if (currWaypoint > 0) {
						currWaypoint--;
					} 
					else 
					{
						patrolForward = true;
						currWaypoint++;
					}
				}
			}
		}
	}


}
