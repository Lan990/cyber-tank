﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManeger : MonoBehaviour {
	public static SoundManeger sound;
	public AudioClip buttonClick;
    public AudioClip gameMusic;
	public AudioClip menuMusic;
	[HideInInspector]
	public AudioSource source;
	[Tooltip("Slider to control volume")]
    public Slider volume;
	[Tooltip("Slider to conrol FX volume")]
	public Slider fxSlider;
	[HideInInspector]
	public float fxVolume;

    void Awake()
    {
		if (sound != null) {
			Destroy (gameObject);
		} else {
			sound = this;
			DontDestroyOnLoad (gameObject);
		}


        source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
    }

    void Start()
    {
        UpdateVolume();
		UpdateFxVolume ();
		PlayMenuMusic ();
    }

	/// <summary>
	/// Plays the click button sound
	/// </summary>
    public void PlayButtonSound()
    {
		source.PlayOneShot(buttonClick, 1.0f);
    }
	/// <summary>
	/// Plays the music for the main menu
	/// </summary>
    public void PlayMenuMusic()
    {
		source.clip = menuMusic;
        source.Play();
        source.loop = true;
    }
	/// <summary>
	/// Plaies the game music.
	/// </summary>
	public void PlayGameMusic()
	{
		source.clip = gameMusic;
		source.Play();
		source.loop = true;
	}
	/// <summary>
	/// Updates the volume = to it's volume slider.
	/// </summary>
    public void UpdateVolume()
    {
        source.volume = volume.value;
    }
	public void UpdateFxVolume()
	{
		fxVolume = fxSlider.value;
	}

}
