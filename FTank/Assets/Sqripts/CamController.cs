﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

    public GameObject player;
    public Vector3 offset = new Vector3(0, 0, -1);
    private Transform tf;//Short hand for camera position.
    private Transform pTf;// Short hand for player position
	public bool twoPCamera;

    void Start()
    {
        tf = gameObject.GetComponent<Transform>();

    }

    void Update()
    {
        if (player)
        {
            // Always Update to Exactly players Position + Offset
            tf.position = new Vector3(pTf.position.x + offset.x, pTf.position.y + offset.y, pTf.position.z + offset.z);
        }
        else
        {
			if (twoPCamera) {
				player = GameManager.gM.secoundPlayer;
				pTf = player.GetComponent<Transform> ();
			} 
			else 
			{
				player = GameManager.gM.player;
				pTf = player.GetComponent<Transform>();
			}
            
        }
    }




}