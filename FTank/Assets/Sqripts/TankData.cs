﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {
	[HideInInspector]
	public Transform tf;
	[HideInInspector]
	public PlayerControl pC;
	[Header("Movement Veriable")]
	public float moveSpeed;
	public float backwordsMoveSpeed;
	public float turnSpeed;
	[Header("Tank Health")]
	public int health;
	public int maxHealth;


	[Header("Shooting")]
	[SerializeField,Tooltip("Amount of time that must pass befor player can shoot again. ")]
	public float fireRate;
	[HideInInspector]
	public float nextFire;
	public float exsplosionDamage=10;





	void Awake()
	{
		tf = GetComponent<Transform> ();
		pC = GetComponent<PlayerControl> ();
	}
	void Update()
	{
		if (health > maxHealth)
			health = maxHealth;
	}
}
