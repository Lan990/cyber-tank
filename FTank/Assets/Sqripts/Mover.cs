﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	[HideInInspector]
	public CharacterController cC;
	private TankData data;

	// Use this for initialization
	void Start () {

		cC = GetComponent<CharacterController> ();
		data = GetComponent<TankData> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	//Function to move character 
	public void Move(Vector3 moveTo)//Gets the move vector from player controler
	{
		cC.SimpleMove (moveTo);
	}
	//Function to rotate character 
	public void Rotate(Vector3 turnTo)//Gets the move vector from player controler
	{
		data.tf.Rotate (turnTo);
	}


	public bool RotateTo(Vector3 target)
	{
		//The diffrence between tank and target.
		Vector3 vectorToTarget=Vector3.zero;
		vectorToTarget= target - data.tf.position;

		Quaternion neededRot = Quaternion.LookRotation (vectorToTarget);// The vector need to rotate to target.
		if (neededRot == data.tf.rotation) {
			return false;
		} 
		else 
		{
			data.tf.rotation = Quaternion.RotateTowards (data.tf.rotation, neededRot, data.turnSpeed * Time.deltaTime);
			return true;
		}
		/*currWaypoint = (int)Mathf.Clamp (currWaypoint, 0, wayPoints.Count - 1);//Selects a waypoint out of list.
		Vector3 tankTurn=Vector3.zero;
		Vector3 moveVector = Vector3.zero;
		tankTurn = wayPoints [currWaypoint].position - data.tf.position;
		SendMessage ("Rotate", tankTurn, SendMessageOptions.DontRequireReceiver);//Calls the rotate function in the mover
		*/
	}


	/*public void Exsplode(gam)
	{
		Destroy()
		//Effects for exsplosion
	}*/

}

