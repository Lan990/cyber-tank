﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManeger : MonoBehaviour {
	public List<PowerUpBase> activePowerups;
	public List<PowerUpBase> deadPowerups;


	private TankData data;
	// Use this for initialization
	void Start () {
		activePowerups = new List<PowerUpBase> ();
		deadPowerups = new List<PowerUpBase> ();
		data = GetComponent<TankData> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (PowerUpBase powerup in activePowerups) //Look hrought the list of active powerups
		{
			powerup.boostDuration -= Time.deltaTime;//Subtract time from their timer.
			if (powerup.boostDuration <= 0) 
			{
				if (powerup.permanent == false) 
				{
					deadPowerups.Add (powerup);
				}
			}
		}

		foreach (PowerUpBase powerup in deadPowerups) 
		{
			powerup.SpeedDeactivate(data,powerup.speedMod);
			activePowerups.Remove (powerup);
		}
		deadPowerups.Clear ();
	}


	public void NewPowerup(PowerUpBase powerup,ModPowerUpType powerUpType)
	{
		if (powerUpType == ModPowerUpType.health) 
		{
			powerup.HealthActivate (data,powerup.healthMod);
		}
		if (powerUpType == ModPowerUpType.speed) 
		{
			powerup.SpeedActivate (data,powerup.speedMod);
		}


		activePowerups.Add (powerup);
	}
}
