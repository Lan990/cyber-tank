﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModPowerUpType{health,speed,nothing};
public class Pickup : MonoBehaviour {

	public ModPowerUpType powerUpType;
    [HideInInspector]
	public PowerUpBase powerup;
	public AudioClip powerAlert;
	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		powerup = GetComponent<PowerUpBase> ();
        Destroy(gameObject,4);
	}
	
	// Update is called once per frame
	void Update () {
		tf.Rotate (1, 0, 0);
	}

	void OnTriggerEnter(Collider other)
	{
		PowerUpManeger powM;
		if(other.GetComponent<PowerUpManeger>())//Check and see if the tank has a powerup maneger
		{
			powM = other.GetComponent<PowerUpManeger> ();//Grab there maneger
			powM.NewPowerup(powerup,powerUpType);


			if (powerAlert != null) 
			{
				AudioSource.PlayClipAtPoint(powerAlert,transform.position,1.0f);
			}
			Destroy (gameObject);
		}
		
	}
}
