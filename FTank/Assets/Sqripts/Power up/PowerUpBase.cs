﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBase: MonoBehaviour {
	//Inhearatince coming I prefer it,
	public float boostDuration=5;
    public float speedMod;
	public int healthMod=5;
	public bool permanent;


	public void HealthActivate(TankData Tank, int healthMod)
	{
		Tank.health += healthMod;
	}

	public void SpeedActivate(TankData Tank,float speedMod)
	{
        Tank.moveSpeed += speedMod;
	}

	public void SpeedDeactivate(TankData Tank, float speedMod)
	{

        Tank.moveSpeed -= speedMod;
	}


}
