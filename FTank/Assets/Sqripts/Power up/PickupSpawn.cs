﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawn : MonoBehaviour {

	private Transform tf;

	[SerializeField,Tooltip("Holder for the prefab of what's being shot")]
	private GameObject[] powerupPrefab;
	public float spawnTime;// Time it takes for a powerup to spawn
	private float nextSpawnTime;//How long till the next spawn
	private GameObject spawnedPickup;


	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		nextSpawnTime = Time.time + spawnTime;
	}
	
	// Update is called once per frame
	void Update () {
		SpawnPowerup ();

	}

	void SpawnPowerup()
	{
		if (spawnedPickup == null) {
			if (Time.time > nextSpawnTime) { //if time past is longer then
				int index= Random.Range(0,powerupPrefab.Length);
				spawnedPickup=Instantiate (powerupPrefab[index], tf.position, Quaternion.identity)as GameObject;
				nextSpawnTime = Time.time + spawnTime;
			}
		} 
		else 
		{
			nextSpawnTime = Time.time + spawnTime;
		}
	}
}
