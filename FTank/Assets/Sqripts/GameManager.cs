﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager gM;

    [Header("Score Tracker")]
    public int score;
    public int highScore;

    private GameObject[] spawnPoints;     //Find the spawn point inthe level
    //private GameObject[] enemySpawns;
    [SerializeField]
    private GameObject[] enemys;
    [HideInInspector]
    public int enemyCount = 0;
    [SerializeField]
    private GameObject playerPrefab;//Player prefab, Can't be private so other sqipts can acess it.
	[SerializeField]
	private GameObject secoundPlayerPrefab;
    [HideInInspector]
    public GameObject player;
	[HideInInspector]
	public GameObject secoundPlayer;
    public int playerLives=1;
    public bool gameOver = false;
	public bool twoP;
    



    // Insures only one of these exsist at a time.
    void Awake()
    {
        gM = this;
        /*if (gM != null)
        {
            Destroy(gameObject);
        }
        else
        {
            gM = this;
            DontDestroyOnLoad(gameObject);
        }*/
        //player = GameObject.Find("Player");


    }

    // Use this for initialization
    void Start()
    {
        
        spawnPoints = GameObject.FindGameObjectsWithTag("Player Spawn");//Opted to only use one spawn point.
        SpawnPlayer();

        Debug.Log("rrr");
        //enemySpawns = spawnPoints;
        SendMessage("LoadJson", SendMessageOptions.DontRequireReceiver);//Calls the function to load highScore data in save data.
		SoundManeger.sound.PlayGameMusic();
    }

    // Update is called once per frame
    void Update()
    {

        if (playerLives > 0)
        {
            SpawnPlayer();
        }
        else
        {
            Time.timeScale = 0;
            SendMessage("gameOver", SendMessageOptions.DontRequireReceiver);//Calls the gameOver function in UI.
            if (score > highScore)
            {
                highScore = score;
                SendMessage("SaveScore", SendMessageOptions.DontRequireReceiver);
            }

        }
        SpawnEnemys();
    }

    /// <summary>
    /// Spawns the player at one of a few spawnpoints.
    /// </summary>
    void SpawnPlayer()
    {

        if (player == null)
        {
            int index = Random.Range(0, spawnPoints.Length);//Gets a random number based off how many spawn points there are.
            
                player = Instantiate(playerPrefab,
                    spawnPoints[index].GetComponent<Transform>().position,
                    spawnPoints[index].GetComponent<Transform>().rotation) as GameObject;
        }
		if (twoP) 
		{
			if (secoundPlayer==null) 
			{
				int index = Random.Range(0, spawnPoints.Length);//Gets a random number based off how many spawn points there are.

				secoundPlayer = Instantiate(secoundPlayerPrefab,
					spawnPoints[index].GetComponent<Transform>().position,
					spawnPoints[index].GetComponent<Transform>().rotation) as GameObject;
			}
		}

    }
    /// <summary>
    /// Spawns enemys based on how many there are in the scene.
    /// </summary>
    void SpawnEnemys()
    {
        if (enemyCount < 4)
        {
            int index = Random.Range(0, enemys.Length);// Gets a random number based off how many enemy types there are.
            int sindex = Random.Range(0, enemys.Length);//Gets a random number based off how many spawn points there are.
            Instantiate(enemys[index], spawnPoints[sindex].GetComponent<Transform>().position,
                spawnPoints[sindex].GetComponent<Transform>().rotation
            );
        }

    }


}
